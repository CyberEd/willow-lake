<?php $title="Sample Menu"; 

$meta = "<meta name=\"description\" content=\"Award winning Chef Louis prepares scones, crêpes, and muffins with gluten-free options at the Willow Lake Bed & Breakfast Inn.\" />";

include("../res/header.php");?>

<div id="content">
<div id="food" class="border">
<div class="container">
<p class="desc">Sweetwater Dining Room<br>Seatings: 8am - noon</p>
</div>
<div class="text">
	<h4 class="center">Premier Course</h4>
	<img class="center" src="/res/images/spacer_menu" alt="" width="250" height="25" />
	<ul>
	<li>Croissant au chocolat</li>
	<li>Pain au amande</li>
	<li>Gourmet butters and jams</li>
	</ul>
	<h4 class="center">Second Course</h4>
	<img class="center" src="/res/images/spacer_menu" alt="" width="250" height="25" />
	<ul>
	<li>Camembert and seasonal berries</li>
	<li>Roquefort fresh fruit compote</li>
	<li>Fromage Blanc & raspberry puree</li>
	</ul>
	<h4 class="center">Grand Entrée</h4>
	<img class="center" src="/res/images/spacer_menu" alt="" width="250" height="25" />
	<ul>
	<li>Quiche Lorraine</li>
	<li>Crêpe Normandy</li>
	</ul>
	<h4 class="center">Beverages</h4>
	<img class="center" src="/res/images/spacer_menu" alt="" width="250" height="25" />
	<ul>
	<li>Orangina Fizz</li>
	<li><a id="tealink" href="/help/js">Organic teas</a>
		<ul id="tea" class="hide">
		<li>White ginger pear</li>
		<li>Lavender citrus</li>
		<li>Vanilla Chai</li>
		</ul>
	</li>
	<li><a id="coffeelink" href="/help/js">Mariage Freres coffees</a>
		<ul id="coffee" class="hide">
		<li>Amaretto spice</li>
		<li>Cappuccino creme</li>
		<li>Espresso avec citron</li>
		</ul>
	</li>
	</ul>
<p class="center">Home of the bottomless &lsquo;Mammoth&rsquo; cookie jar & tea pot.</p>
<br>
</div>
</div>


</div>

<script>
var coffee = document.getElementById("coffee");
var tea = document.getElementById("tea");
var teaLink = document.getElementById("tealink");
var coffeeLink = document.getElementById("coffeelink");
var coffeeOn = false;
var teaOn = false;
	coffeeLink.onclick = function(){
		if (coffeeOn == false){
			coffee.className = "";
			tea.className = "hide";
			coffeeOn = true;
			teaOn = false;
		}
		else{
			coffee.className = "hide";
			coffeeOn = false;
		}
		return false;
	}
	teaLink.onclick = function(){
		if (teaOn == false){
			tea.className = "";
			coffee.className = "hide";
			teaOn = true;
			coffeeOn = false;
		}
		else{
			tea.className = "hide";
			teaOn = false;
		}
		return false;
	}
</script>

<?php include("../res/footer.php"); ?>
