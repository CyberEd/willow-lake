<?php $title="Breakfast";

$meta = "<meta name=\"description\" content=\"At Willow Lake Bed & Breakfast Inn, breakfast is the most important meal of the day. Flavorful, health conscious meals are prepared daily.\" />";

include("../res/header.php");?>

<div id="content">

<p><span class="dropcap">A</span>t Willow Lake Bed and Breakfast, we truly 
believe breakfast is the most important meal of the day! Our unique,
elegantly presented dishes are bursting with <strong>flavor</strong>. The health of our 
guests is always considered when preparing a delicious, well balanced meal.
All our fruits, vegetables and meats are obtained locally. Many of our 
decadent breads and cakes are <strong>gluten-free</strong>.</p>

<div class="center">
<img class="border" src="/res/images/dining" alt="A typical breakfast in the Sweetwater Dining Room" title="A typical breakfast in the Sweetwater Dining Room" width="340" height="420" />
</div>

<p>Each morning, a <strong>three course</strong> meal is served with an alternating 
assortment of steaming hot scones, crêpes, muffins, or other baked 
goodies.</p>

<p><strong>Seasonal fruits</strong> create the refreshing second course. Specialties include
homemade camembert and roquefort cheeses nestled amongst succulent berries
and juicy pears. Ask about our famous &ldquo;Fromage Blanc&rdquo; with 
raspberry sauce.</p>

<p>Lastly, our <strong>hot entrée</strong> is served. Selections vary, but usually include a 
range free egg dish such as quiche Lorraine or eggs with black truffles.
Additionally, included is your choice of lean meats or a vegetarian side
dish. Dietary preferences are always accommodated.</p>

<p>Top off your meal with a sparkling &ldquo;Orangina Fizz&rdquo; made with
fresh squeezed juices and natural sparkling spring water from our mineral 
springs. Many allege it has curative powers! Of course, only the <strong>finest 
coffees</strong> and teas make their way onto your table.</p>

<h3>Bon appétit,<br>Chef Louis & Staff</h3>

<div class="center"><a href="menu">Sample Menu</a> &middot; <a href="recipe">Recipes</a></div>
</div>

<?php include("../res/footer.php"); ?>
