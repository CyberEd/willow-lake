<?php $title="Recipes"; include("../res/header.php");?>

<div id="content">


<div id="recipe">
<div id="text">

<!--Tab buttons-->
<a id="tab1" href="/help/js" style="width:55px; height:55px; left: 117px; top: -65px;"></a>
<a id="tab2" href="/help/js" style="width:50px; height:75px; left: 455px; top: -90px;"></a>
<a id="tab3" href="/help/js" style="width:50px; height:60px; left: 590px; top: -65px;"></a>

<!--Arrow Overlays-->
<img id="leftArrow" class="hide" src="/res/images/buttons/leftarrow" width="50" height="50" alt="Left Arrow" />
<img id="rightArrow" class="hide" src="/res/images/buttons/rightarrow" width="50" height="50" alt="Right Arrow" />

<div id="leftSide" class="leftcontent" onmouseover="showLeftArrow()" onmouseout="hideLeftArrow()" >
<h2>Gramma's Pumpkin Pancakes</h2>
<h4>Makes sixteen small</h4>
<ul>
	<li>2 C biscuit mix</li>
	<li>2 T light brown sugar</li>
	<li>2 tsp cinnamon</li>
	<li>1 &frac12; C evap. milk</li>
	<li>1 tsp allspice</li>
	<li>1 &frac12; C pumpkin</li>
	<li>2 eggs</li>
	<li>1 tsp vanilla</li>
</ul>
<p>Mix ingredients and pour onto hot griddle.</p>
</div>

<div id="rightSide" class="rightcontent" onmouseover="showRightArrow()" onmouseout="hideRightArrow()" >
	<img class="center" src="/res/images/recipes/pancakes" alt="Gramma's delicious pancake breakfast" width="300" height="230" />
</div>
</div>

<div id="pg1" class="hide">
<h2>Gramma's Pumpkin Pancakes</h2>
<h4>Makes sixteen small</h4>
<ul>
	<li>2 C biscuit mix</li>
	<li>2 T light brown sugar</li>
	<li>2 tsp cinnamon</li>
	<li>1 &frac12; C evap. milk</li>
	<li>1 tsp allspice</li>
	<li>1 &frac12; C pumpkin</li>
	<li>2 eggs</li>
	<li>1 tsp vanilla</li>
</ul>
<p>Mix ingredients and pour onto hot griddle.</p>
</div>
<div id="pg2" class="hide">
	<img class="center" src="/res/images/recipes/pancakes" alt="Gramma's delicious pancake breakfast" width="300" height="230" />
</div>
<div id="pg3" class="hide">
<h2>Quiche Lorraine</h2>
<h4>Serves six</h4>
<ul>
	<li>4 eggs</li>
	<li>1 pt half n' half</li>
	<li>Deep dish pie crust</li>
	<li>Swiss cheese slices</li>
	<li>1 C cooked spinach</li>
	<li>1 C cooked bacon</li>
	<li>Shredded Gruyère</li>
</ul>
<p>Line pie crust with sliced cheese. Beat eggs, cream. Place bacon,
spinach, and shredded cheese in crust. Pour in filling. Bake 350&deg; for 45 minutes.</p>
</div>
<div id="pg4" class="hide">
	<img class="top" src="/res/images/recipes/quiche" alt="Freshly baked quiche" width="270" height="340" />
</div>
<div id="pg5" class="hide">
<h2>Maple Cinnamon Scones</h2>
<h4>Serves six</h4>
<ul>
	<li>1 C flour</li>
	<li>2 T sugar</li>
	<li>1 tsp baking powder</li>
	<li>1 tsp cinnamon</li>
	<li>&frac14; tsp salt</li>
	<li>&frac14; C butter in small pieces</li>
	<li>&frac14; C heavy cream</li>
	<li>2 T maple syrup</li>
	<li>1 egg beaten</li>
	<li>&frac12; C chopped pecans</li>
</ul>
</div>
<div id="pg6" class="hide">
<p>Place first 5 ingredients in bowl, cut in butter to form coarse crumbs.
Stir in remaining ingredients. Drop dough onto pan to make six scones. Bake
at 300&deg; for 15 minutes. Serve with maple butter and homemade raspberry 
jam.</p>
<img src="/res/images/recipes/scones" alt="Maple Cinnamon Scones" width="175" height="175" />
</div>
<div id="pg7" class="hide">
<h2>Mammoth Chip Cookies</h2>
<h4 class="right" >Gluten-free</h4>
<h4>Serves twelve</h4>
<ul>
	<li>&frac12; C butter at room temp</li>
	<li>&frac14; C sugar</li>
	<li>&frac12; C packed light brown sugar</li>
	<li>1 tsp vanilla extract</li>
	<li>1 large egg</li>
	<li>1 C + 2 T gluten-free baking mix</li>
	<li>1 C semisweet chocolate chips</li>
</ul>
</div>
<div id="pg8" class="hide">
<p>Preheat oven to 350&deg;. Cream butter and sugar in large mixing bowl. 
Add vanilla and egg. Beat until well combined. Add gluten-free mix until 
mixture appears doughy. Stir in chocolate chips. Refrigerate 4-6 hours. Roll
tablespoons of dough into balls. Bake 10 minutes.</p>
<img class="bottom" src="/res/images/recipes/cookies" alt="Famous mammouth cookie" width="160" height="220" />
</div>
<script>
var page=1;
window.onload = function() {
var right=document.getElementById("rightSide");
var left=document.getElementById("leftSide");

right.style.cursor = 'pointer';
left.style.cursor = 'pointer';
right.onclick = nextPage;
left.onclick = prevPage;
function nextPage(){
	if (page < 7){
		page += 2;
		jumpTo();
	}
}
function prevPage(){
	if (page > 1){
		page -= 2;
		jumpTo();
	}
}
function jumpTo(position){
	if(position){
		if(isOdd(position) == true){
			page = position;
		}
		if(isOdd(position) == false){
			page = (position - 1);
		}
	}
	pageR = page + 1;
	left.innerHTML=document.getElementById('pg' + page).innerHTML;
	right.innerHTML=document.getElementById('pg' + pageR).innerHTML;
	hideLeftArrow();
	hideRightArrow();
}
function isOdd(num) {
	return num % 2;
}

//tabs
var tab1 = document.getElementById("tab1");
var tab2 = document.getElementById("tab2");
var tab3 = document.getElementById("tab3");
tab1.onclick = function(){ jumpTo(3); return false; }
tab2.onclick = function(){ jumpTo(5); return false; }
tab3.onclick = function(){ jumpTo(7); return false; }
}
//arrows
var leftArrow = document.getElementById("leftArrow");
var rightArrow = document.getElementById("rightArrow");

function showLeftArrow(){
	if(page > 2){
		leftArrow.className = "leftarrow";
	}
}
function hideLeftArrow(){
	leftArrow.className = "hide";
}
function showRightArrow(){
	if(page < 6){
		rightArrow.className = "rightarrow";
	}
}
function hideRightArrow(){
	rightArrow.className = "hide";
}

</script>

</div>
</div>
<?php include("../res/footer.php"); ?>
