<?php

//keep names consistent
function displayName($param){
	switch($param){
	case "meadowlark":
		return("The Meadowlark");
		break;
	case "retreat":
		return("Ed's Retreat");
		break;
	case "star":
		return("Shooting Star");
		break;
	case "honeymoon":
		return("Golden Nugget");
		break;
	case "aspen":
		return("Aspen Suite");
		break;
	case "catkin":
		return("Catkin Silver");
		break;
	case "sun":
		return("Morning Sun");
		break;
	case "primrose":
		return("Evening Primrose");
		break;
	}
}

//keep prices consistent
function findRate($room){
	switch($room){
		case "aspen":
			return "129";
			break;
		case "catkin":
			return "119";
			break;
		case "sun":
			return "110";
			break;
		case "primrose":
			return "115";
			break;
		case "retreat":
			return "159";
			break;
		case "meadowlark":
			return "149";
			break;
		case "star":
			return "145";
			break;
		case "honeymoon":
			return "219"; 
			break;
	}
}

function totalCost($room, $arriveBlob, $departBlob){
	$days = ($departBlob - $arriveBlob);
	$cost = ($days * findRate($room));
	return($cost);
}

//create URL to commit form w/ proper data
//requires vars:
//	$rooom, $arriveBlob, $departBlob
function writeURL($room, $arriveBlob, $departBlob, $linkify = false, $useBull = false){
	$str = "/reserve/commit?room=$room&amp;arrive=$arriveBlob&amp;depart=$departBlob";
	if($linkify){
		$str = "<a href=\"" . $str . "\" >" . displayName($room) . "</a>";
	}
	if($useBull){
		$str = "<li>" . $str . "</li>";
	}
	return($str);
}

?>
