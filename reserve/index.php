<?php $title="Reservations"; 

//include meta tag AND calendar style sheet (so it is in <head>)
$meta = "<meta name=\"description\" content=\"Willow Lake Bed & Breakfast Inn welcomes you. Great rates by phone or online.\" />
<link rel=\"stylesheet\" href=\"http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css\">";

include("../res/header.php"); 

//look for room selection
function roomCheck(){
	if ($_GET[room]){
		echo($_GET[room]);
	}else{
		echo("null");
	}
}
?>

<div id="content">

<p><span class="dropcap">W</span>e will gladly check our online schedule and show current availability.</p>

<div id="error" class="hide"></div>

<form name="myForm" action="lookup" onsubmit="return validateForm()" method="post">
<label>Arrival: </label><input type="text" name="arrive" id="datestart" value="mm/dd/yyyy" onfocus="clearBox(this)" onblur="resetBox(this)" onchange="resetBox(this)"/><br>
<label>Departure: </label><input type="text" name="depart" id="dateend" value="mm/dd/yyyy" onfocus="clearBox(this)" onblur="resetBox(this)" onchange="resetBox(this)"/><br>
<input type="hidden" name="room" value="<?php roomCheck(); ?>" />
<div class="offset">
<div class="centerfloat">
<input style="position: relative; left: 10px;" type="submit" value="Check Availability" />
</div>
</div>
</form>

<br>

<h3>Our Policy</h3>
<ul>
	<li>For the comfort of our guests, Willow Lake B&amp;B is 
	non-smoking.</li>
	<li>Pets are welcome in Lower Level and require a $50 deposit.</li>
	<li>Check-in is after 4:00pm. For arrivals after 8:00pm, call for
	advanced arrangements.</li>
	<li>Check-out is 1:00pm on the day of departure.</li>
	<li>Cancellations of confirmed reservations must be made at least
	36 hours in advance.</li>
	<li>A first night, or 50% (whichever is more) deposit is 
	required.</li>
</ul>

<h3>Room Tariffs & Rates</h3>
<ul>
	<li>Guest rooms range from $110 - 219/night + tax.</li>
	<li>Rates include lodging, afternoon tea social, 24 hour 
	coffee/tea, bottomless &lsquo;Mammoth&rsquo; cookie jar, and 3 
	course gourmet breakfast.</li>
	<li>Call for extended stay rates and packages.</li>
	<li>Call to schedule special occasions and events in the 
	Sweetwater Dining Room or Eve's Garden.</li>
</ul>

</div>

<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script>
//calendar widget
$(function() {
	$( "#datestart" ).datepicker({
		showOn: "both",
		buttonImage: "/res/images/buttons/calendar",
		buttonImageOnly: true
	});
});
$(function() {
	$( "#dateend" ).datepicker({
		showOn: "both",
		buttonImage: "/res/images/buttons/calendar",
		buttonImageOnly: true
	});
});

//watermark
function clearBox(inputBox){
	if(inputBox.value == "mm/dd/yyyy"){
		inputBox.style.color="#000";
		inputBox.value = "";
	}
}
function resetBox(inputBox, resetAll){
		if(inputBox.value == ""){
			inputBox.style.color="#aaa";
			inputBox.value = "mm/dd/yyyy";
		}else{
			inputBox.style.color="#000";
		}
}
function styleBox(inputBox){
	inputBox.style.color="#aaa";
}

//validation
function validateForm()
{
	var errorBox = document.getElementById("error");
	var arriveRaw = document.forms["myForm"]["arrive"].value;
	var departRaw = document.forms["myForm"]["depart"].value;
	var arriveSplit = arriveRaw.split("/");
	var departSplit = departRaw.split("/");
	var arrive = new Date(document.forms["myForm"]["arrive"].value);
	var depart = new Date(document.forms["myForm"]["depart"].value);
	var current = new Date().getTime();
	if ((arriveRaw.split("/").length - 1) != 2 || arriveSplit[0].length != 2 || arriveSplit[1].length != 2 || arriveSplit[2].length != 4){
		error.innerHTML = "Invalid arrival date format. Correct format is mm/dd/yyyy.";
		error.className = "error";
		return false;
	}else if ((departRaw.split("/").length - 1) != 2 || departSplit[0].length != 2 || departSplit[1].length != 2 || departSplit[2].length != 4){
		error.innerHTML = "Invalid departure date format. Correct format is mm/dd/yyyy.";
		error.className = "error";
		return false;
	}else if (arriveRaw == null || arriveRaw == "" || arriveRaw == "mm/dd/yyyy"|| departRaw == null || departRaw == "" || departRaw == "mm/dd/yyyy"){
		error.innerHTML = "Please ensure both dates are complete.";
		error.className = "error";
		return false;
	}else if (depart.valueOf() <= arrive.valueOf()){
		error.innerHTML = "Departure date must be after your arrival.";
		error.className = "error";
		return false;
	}else if (current.valueOf() > arrive.valueOf()){
		error.innerHTML = "Please book a room for the future. That date has already passed.";
		error.className = "error";
		return false;
	}
}

window.onload = function(){
	box1 = document.getElementById("datestart");
	box2 = document.getElementById("dateend");
	if(box1.value == "mm/dd/yyyy"){
		styleBox(box1);
	}
	if(box2.value == "mm/dd/yyyy"){
		styleBox(box2);
	}
}
</script>

<?php include("../res/footer.php"); ?>
