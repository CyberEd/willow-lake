<?php $title="Room Finder"; include("../res/header.php");?>

<div id="content">

<div class="rightdiv"><img src="/res/images/wifi" alt="Free Wifi available in all rooms" width="170" height="170" /></div>
<div class="text">
<?php
require('../res/config.php');
require('api.php');

//initialize room array
$isFull = array(
	"meadowlark"=>false,
	"retreat"=>false,
	"star"=>false,
	"honeymoon"=>false,
	"aspen"=>false,
	"catkin"=>false,
	"sun"=>false,
	"primrose"=>false
	);

$arrive = $_POST["arrive"];
$depart = $_POST["depart"];
$preselect[0] = $_POST["room"];
$preselect[1] = null;
$preselect[2] = displayName($preselect[0]);

$showAllRooms = true;

$arrive = array_map('trim',explode("/",$arrive));
$depart = array_map('trim',explode("/",$depart));

$i = 0; //reset counter

//create blobs
$arriveBlob = "$arrive[2]"."$arrive[0]"."$arrive[1]";
$departBlob = "$depart[2]"."$depart[0]"."$depart[1]";

//0 => month 1 => day 2 => year

//sanity checks
if($_POST["arrive"] == "mm/dd/yyyy" || $_POST["arrive"] == "" || $_POST["depart"] == "mm/dd/yyyy" || $_POST["depart"] == ""){
	echo "<p>Please choose both an arrival and departure date.</p>";
}
else if(strlen($arrive[0]) != 2 || strlen($depart[0]) != 2 
   || strlen($arrive[1]) != 2 || strlen($depart[1]) != 2 
   || strlen($arrive[2]) != 4 || strlen($depart[2]) != 4 )
{
	echo "<p>Invalid date format.</p>";
}else if($departBlob <= $arriveBlob){
	echo "<p>Departure date must be after your arrival.</p>";
}else{
//connect to database
$con = mysqli_connect($mysql['host'], $mysql['username'], $mysql['password'], $mysql['dbname']);
//ensure connection
if (mysqli_connect_errno()){
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

//subtract 1 - departure and arrival can be equal - makes lookup easy
$departCheck = $departBlob - 1;

//start DB calls
$result = mysqli_query($con,"SELECT * FROM reservations WHERE Arrive BETWEEN $arriveBlob AND $departCheck OR $arriveBlob BETWEEN Arrive AND Depart");

//print_r($result);
//echo "<br>";
while($row = mysqli_fetch_array($result)){
	switch($row['Room']){
		case "meadowlark":
			$isFull["meadowlark"] = true;
			break;
		case "retreat":
			$isFull["retreat"] = true;
			break;
		case "star":
			$isFull["star"] = true;
			break;
		case "honeymoon":
			$isFull["honeymoon"] = true;
			break;
		case "aspen":
			$isFull["aspen"] = true;
			break;
		case "catkin":
			$isFull["catkin"] = true;
			break;
		case "sun":
			$isFull["sun"] = true;
			break;
		case "primrose":
			$isFull["primrose"] = true;
			break;
	}
}

if (!in_array(false, $isFull)){
	echo "<p>We're sorry. We have no opennings for the requested dates.</p>";
}else{
	//check if preselected room
	if ($preselect[0] == 'null' || $preselect[0] == ''){
		echo "Rooms available:<br>";
	}else{
		if($isFull[$preselect[0]] == true){
			echo("<p>We're sorry, $preselect[2] is sold out for the selected dates.</p>
			      <p>Please choose an alternate date or an available room listed below.</p>
			      ");
		}else{
			echo("<p>You have selected $preselect[2].<br><a href='" . writeURL($preselect[0], $arriveBlob, $departBlob) . "'><img src='/res/images/buttons/continue' alt='Continue Button' width='110' height='50' /></a></p>");
			$showAllRooms = false;
		}
	}
	if($showAllRooms == true){
		echo "<ul>";
		//cycle thru rooms
		if($isFull["meadowlark"] == false){
			echo(writeURL("meadowlark", $arriveBlob, $departBlob, true, true));
		}
		if($isFull["retreat"] == false){
			echo(writeURL("retreat", $arriveBlob, $departBlob, true, true));
		}
		if($isFull["star"] == false){
			echo(writeURL("star", $arriveBlob, $departBlob, true, true));
		}
		if($isFull["honeymoon"] == false){
			echo(writeURL("honeymoon", $arriveBlob, $departBlob, true, true));
		}
		if($isFull["aspen"] == false){
			echo(writeURL("aspen", $arriveBlob, $departBlob, true, true));
		}
		if($isFull["catkin"] == false){
			echo(writeURL("catkin", $arriveBlob, $departBlob, true, true));
		}
		if($isFull["sun"] == false){
			echo(writeURL("sun", $arriveBlob, $departBlob, true, true));
		}
		if($isFull["primrose"] == false){
			echo(writeURL("primrose", $arriveBlob, $departBlob, true, true));
		}
		echo "</ul>";
	}
}
//close connection to DB
mysqli_close($con);
}
?>
<img src="/res/images/spacer.png" alt="" height="100" width="0" />
</div>
</div>
<?php include("../res/footer.php"); ?>
