<?php $title="Payment Info";  include("../res/header.php");?>

<div id="content">

<p><span class="dropcap">P</span>lease complete the information below to
reserve <strong>
<?php 
require("api.php");
echo(displayName($_GET[room]));
?>
</strong> for the dates provided.</p>

<form action="post?room=<?php echo $_GET[room]; ?>&amp;depart=<?php echo $_GET[depart]; ?>&amp;arrive=<?php echo $_GET[arrive]; ?>" method="post">
<p><strong>How should we greet you?</strong></p>
<label>*Name: </label><input type="text" name="name" /><br>
<label>*Email: </label><input type="text" name="email" /><br>

<p><strong>Main Residence</strong></p>
<label>Address: </label><input type="text" disabled/><br>
<label>City: </label><input type="text" disabled/><br>
<label>State: </label><input type="text" disabled/><br>
<p><strong>Payment Info</strong></p>
<label>Type:</label> 
<select name="cardType" disabled>
	<option>American Express</option>
	<option>Mastercard</option>
	<option>Visa</option>
</select>
<br>
<label>Number: </label><input type="text" disabled/><br>
<label>CVC: </label><input type="text" disabled/>
<div id="total" class="border">
<p style="margin-right: 15px;">
<?php
	$sub = totalCost($_GET[room], $_GET[arrive], $_GET[depart]);
	$tax = ($sub * .1);
	$total = ($sub + $tax);
	echo "<label>Subtotal:</label> $". number_format($sub, 2). "<br>";
	echo "<label>Taxes:</label> $" . number_format($tax, 2). "<br>";
	echo "<strong><label>Total:</label> $" . number_format($total, 2) . "</strong>";
?>
</p>
<input class="large" type="image" src="/res/images/buttons/confirm.png" alt="Confirm" style="margin-left: 50px;" />
</div>
</form>
<br>

</div>
<script>
window.onload = function() {

	function getScrollTop() {
		if (typeof window.pageYOffset !== 'undefined' ) {
			// Most browsers
			return window.pageYOffset;
		}

		var d = document.documentElement;
		if (d.clientHeight) {
			// IE in standards mode
			return d.scrollTop;
		}

		// IE in quirks mode
		return document.body.scrollTop;
	}

	window.onscroll = function() {
		var box = document.getElementById('total'),
		    scroll = getScrollTop();
		if (scroll <= 300){
			if (scroll <= 40) {
				box.style.top = "80px";
			}
			else {
				box.style.top = (scroll + 3) + "px";
			}
		}
	};
};
</script>
<?php include("../res/footer.php"); ?>
