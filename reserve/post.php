<?php $title="Thank You!"; include("../res/header.php"); ?>

<div id="content">

<?php
//get mysql conf
include_once('../res/config.php');

//get vars
$room = $_GET['room'];
$departBlob = $_GET['depart'];
$arriveBlob = $_GET['arrive'];
//post vars
$name = $_POST['name'];
$email = $_POST['email'];

//sanity checks
if ($room == '' || $departBlob == '' || $arriveBlob == ''){
	echo("Oops! Looks like something was lost. Please <a href='/reserve/'>try again</a>.");
}else if($email == '' || $name == ''){
	echo("Please fill out all fields on the previous page.");
}
else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	echo("Please input a valid email address.");
}else{
//connect to database
$con = mysqli_connect($mysql['host'], $mysql['username'], $mysql['password'], $mysql['dbname']);
//ensure connection
if (mysqli_connect_errno()){
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
//CONFIRM THAT THE ROOM IS AVAILABLE BEFORE BOOKING & SANATIZE

//book room if no errors
$query = "INSERT INTO reservations (Name, Room, Arrive, Depart) VALUES ('$name', '$room', '$arriveBlob', '$departBlob');";
//echo "$query";
$result = mysqli_query($con, $query);

$query = "SELECT Id, Room FROM reservations WHERE Arrive = $arriveBlob AND Depart = $departBlob AND Name = '$name';";
$result = mysqli_query($con, $query);
$result = mysqli_fetch_array($result);

//send email confirmation
$confirmation = ($result[Id] + 1000);

$to = $email;
$from = $MAIL[from];
$subject = "Willow Lake Reservations";
$message = "Thank you for your reservation. \r\n
Your confirmation number is " . $confirmation . ".\r\n
We look forward to seeing you soon!\r\n
\r\n
Room: " . $result[Room];

$headers = "From:" . $from;
mail($to,$subject,$message,$headers);

echo "<p><span class='dropcap'>T</span>hank you, we look forward to seeing you soon!</p>
<p>A confirmation email will arrive shortly. Please check your spam folder.";
}
?>

</div>
<?php include("../res/footer.php"); ?>
