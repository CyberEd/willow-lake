<?php $title="Out & About"; 

$meta="<meta name=\"description\" content=\"The fun never ends at Willow Lake Bed & Breakfast Inn; a playground for nature's wildlife and the sports enthusiast. This resort has it all!\" />";

include("../res/header.php");?>

<div id="content">

<div id="slider">
<ul>
	<li><img src="/res/images/info/skiing.jpg" alt="Family Skiing" height="320" width="620" /></li>
	<li><img src="/res/images/info/dogsled.jpg" alt="Dogledding" height="320" width="620" /></li>
	<li><img src="/res/images/info/snowball.jpg" alt="Enjoying the Hot Springs" height="320" width="620" /></li>
	<li><img src="/res/images/info/owl.jpg" alt="Twin Owl Sighting" height="320" width="620" /></li>
	<li><img src="/res/images/info/moose.jpg" alt="Moose Sighting" height="320" width="620" /></li>
	<li><img src="/res/images/info/fish.jpg" alt="Fishing at a River" height="320" width="620" /></li>
	<li><img src="/res/images/info/pheasant.jpg" alt="Pheasant Sighting" height="320" width="620" /></li>
	<li><img src="/res/images/info/golf.jpg" alt="Nearby Golf Course" height="320" width="620" /></li>
	<li><img src="/res/images/info/kayak.jpg" alt="Kayaking" height="320" width="620" /></li>
	<li><img src="/res/images/info/horse.jpg" alt="Horseback Riding" height="320" width="620" /></li>
</ul>
<span class="arrow previous"></span>
<span class="arrow next"></span>
</div>

<br>

<p><span class="dropcap">T</span>he Willow Lake area is a playground for 
nature's wildlife and the sports enthusiast. Consider a leisurely walk 
around the lake or a rambling horseback ride down Catkin Trail. The 
prestigious nearby 
<a href="http://www.thepowderhorn.com/sites/courses/layout9.asp?id=541&amp;page=27858" target="_blank">Powder Horn Golf Course</a> offers beautiful views 
along with first class dining. Paddle boats and kayaks are available
for a day of boating. If fishing is your passion, you'll be in your
glory reeling in the largest paddle fish in the west!</p>

<p>Love the winter? <a href="http://www.lodgesofthebighorns.com/meadowlarkskilodge.html" target="_blank">Meadowlark Ski Area</a> is just a short drive away. Try 
dogsledding for a change of pace. You drive your own team for a truly once 
in a lifetime adventure.</p>

<p>The downtown area features quaint, western themed shops and
authentic restaurants.</p>

<h3>Be sure to ask for Miles at the front desk who can provide you with 
additional information and help plan your perfect getaway.</h3>
<br>
<p>May we also recommend these fine dining options:</p>
<ul>
	<li><a href="http://www.blacktoothbrewingcompany.com/" target="_blank">Blacktooth Brewing Company</a></li>
	<li><a href="http://www.ribandchophouse.com/" target="_blank">Rib & Chop House</a></li>
	<li><a href="http://www.wagonbox.com/" target="_blank">Wagon Box</a></li>
</ul>

</div>
<!--Slide Show Scripts-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script src="/res/scripts/slider.js"></script>
<script src="/res/scripts/sliderauto.js"></script>
<!--End Slideshow-->
<?php include("../res/footer.php"); ?>
