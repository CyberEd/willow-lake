<?php $title="Gift Shop"; include("../res/header.php");?>

<div id="content">

<iframe class="centerfloat" width="560" height="315" src="//www.youtube.com/embed/7DBiS5HUk1g" frameborder="0" allowfullscreen></iframe>

<p><span class="dropcap">A</span>&nbsp;visit to Willow Lake 
would not be complete without a stroll over to the <strong>Tweetheart 
Emporium</strong>. This gift shop museum specializes in one of a kind 
<strong>antiques</strong> and vintage <strong>collectibles</strong>. 
Let Anne assist you in selecting that perfect token to commemorate 
your stay at Willow Lake Bed & Breakfast Inn.</p>

<!--Hours-->
<div class="centerfloat">
Hours<br><img src="/res/images/spacer_menu" alt="" width="300" height="20" />
<br>
<table class="center"> 
<tr>
<td>Mon - Fri</td><td class="left">8 - 5</td>
</tr><tr>
<td>Sat</td><td class="left">9 - 6</td>
</tr><tr>
<td>Sun</td><td class="left">11 - 4</td>
</tr>
</table>
</div>

<h3>Come on in for a cup of tea and a Mammoth Chip Cookie!</h3>

</div>
<?php include("../res/footer.php"); ?>
