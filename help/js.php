<?php $title="Javascript is Disabled"; include("../res/header.php");?>

<div id="content">

<h2>Sorry!</h2>
<br>
<p><span class="dropcap">T</span>he content you tried to view requires JavaScript. <a href="http://enable-javascript.com/" target="_blank">More info please!</a></p>

</div>
<?php include("../res/footer.php"); ?>
