<?php $title="Upper Level"; include("../res/header.php");?>

<div id="content">

<h3 class="rightf"><a href="/reserve/"><img src="/res/images/buttons/reserve" width="145" height="60" alt="Reservations" /></a></h3>
<h2>Upper Level</h2>

<div id="rows">
<div class="bounds leftf">
<h2>Ed's Retreat</h2>
<ul>
	<li>Four twin beds</li>
	<li>Large screen TV</li>
	<li>Kids' hideaway</li>
</ul>
</div>
<img class="bounds leftf border" src="/res/images/accommodations/hideaway" alt="Ed's Retreat" width="230" height="160" />
<div class="bounds leftf center pull"><span>$159</span><br><a href="/reserve/?room=retreat"><img src="/res/images/buttons/reserveblack" width="100" height="50" alt="Reserve Ed's Retreat" /></a></div>
</div>

<div id="rows">
<div class="bounds leftf">
<h2>The Meadowlark</h2>
<ul>
	<li>Queen bed</li>
	<li>Lake view</li>
	<li>Private bath</li>
</ul>
</div>
<img class="bounds leftf border" src="/res/images/accommodations/meadowlark" alt="The Meadowlark" width="230" height="160" />
<div class="bounds leftf center pull"><span>$149</span><br><a href="/reserve/?room=meadowlark"><img src="/res/images/buttons/reserveblack" width="100" height="50" alt="Reserve The Meadowlark" /></a></div>
</div>

<div id="rows">
<div class="bounds leftf">
<h2>Shooting Star</h2>
<ul>
	<li>Double bed</li>
	<li>Mountain view</li>
	<li>Private bath</li>
</ul>
</div>
<img class="bounds leftf border" src="/res/images/accommodations/star" alt="Shooting Star" width="230" height="160" />
<div class="bounds leftf center pull"><span>$145</span><br><a href="/reserve/?room=star"><img src="/res/images/buttons/reserveblack" width="100" height="50" alt="Reserve Shooting Star" /></a></div>
</div>
<p class="center"><a href="lower-level">Lower Level</a> · <a href="honeymoon-suite">Honeymoon Suite</a></p>
</div>

<?php include("../res/footer.php"); ?>
