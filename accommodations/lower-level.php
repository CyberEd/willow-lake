<?php $title="Lower Level"; include("../res/header.php");?>

<div id="content">

<h3 class="rightf"><a href="/reserve/"><img src="/res/images/buttons/reserve" width="145" height="60" alt="Reservations" /></a></h3>
<h2>Lower Level</h2>
<p>Our affordable, convenient lower level rooms feature:</p>

<ul>
	<li>Four bedrooms</li>
	<li>Two shared baths</li>
	<li>Various configurations</li>
	<li>Easy access
		<ul>
		<li>ADA-Approved</li>
		<li>TTY Phones</li>
		</ul>
	</li>
</ul>

<br>

<div id="dynamic" class="block">
	<img class="border leftf" src="/res/images/accommodations/bathroom2" alt="Bathroom" width="385" height="300" />
	<img class="border rightf" src="/res/images/accommodations/aspen" alt="Aspen Room" width="385" height="300"/>
</div>

<div id="carouselcontent" class="hide">

<span class="prev">&lt;&lt;</span>
<span class="next">>></span>

<ul>
	<li>
	<img class="border" src="/res/images/accommodations/bathroom2" alt="Bathroom" width="385" height="300" />
	</li>
	<li>
	<img class="border" src="/res/images/accommodations/aspen" alt="Aspen Room" width="385" height="300"/>
	</li>
	<li>
	<img class="border" src="/res/images/accommodations/silvermoon" alt="Silvermoon Room" width="385" height="300"/>
	</li>
	<li>
	<img class="border" src="/res/images/accommodations/sunburst" alt="Sunburst Room" width="385" height="300"/>
	</li>
	<li>
	<img class="border" src="/res/images/accommodations/bathroom" alt="Second Bathroom" width="385" height="300"/>
	</li>
	<li>
	<img class="border" src="/res/images/accommodations/primrose" alt="Primrose Room" width="385" height="300" />
	</li>
</ul>

</div>

<div id="squares">
<div class="bounds leftf center aspen">
<h2>Aspen Suite</h2>
	Two double beds<br>
	Sleeps four<br>
	$129<br>
	<br>
<a href="/reserve/?room=aspen"><img src="/res/images/buttons/reserveblack" width="90" height="35" alt="Reserve Aspen Suite" /></a>

</div>
<div class="bounds rightf center noleft catkin" style="float: right;">
<h2>Catkin Silver</h2>
	One king bed<br>
	One sofa sleeper<br>
	Sleeps three<br>
	$119<br>
<a href="/reserve/?room=catkin"><img src="/res/images/buttons/reserveblack" width="90" height="35" alt="Reserve Catkin Silver" /></a>
</div>
<div class="bounds leftf center notop sun">
<h2>Morning Sun</h2>
	One double bed<br>
	Sleeps two<br>
	$110<br>
	<br>
<a href="/reserve/?room=sun"><img src="/res/images/buttons/reserveblack" width="90" height="35" alt="Reserve Morning Sun" /></a>
</div>
<div class="bounds rightf center notop noleft primrose">
<h2>Evening Primrose</h2>
	One queen bed<br>
	Sleeps two<br>
	Accommodates crib or cot<br>
	$115<br>
<a href="/reserve/?room=primrose"><img src="/res/images/buttons/reserveblack" width="90" height="35" alt="Reserve Evening Primrose" /></a>
</div>
</div>
<p class="center"><a href="upper-level">Upper Level</a> · <a href="honeymoon-suite">Honeymoon Suite</a></p>

</div>
<script type="text/javascript" src="/res/scripts/jquery.js"></script>
<script type="text/javascript" src="/res/scripts/jcarousellite.js"></script>

<script>
var content = document.getElementById("carouselcontent");
var box = document.getElementById("dynamic");
box.id = "carousel";
box.className = "carousel";
box.innerHTML = content.innerHTML;

$(function() {
    $(".carousel").jCarouselLite({
        btnNext: ".next",
        btnPrev: ".prev"
    });
});
</script>

<?php include("../res/footer.php"); ?>
