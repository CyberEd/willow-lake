<?php $title="Honeymoon Suite"; 

$meta = "<meta name=\"description\" content=\"The Gold Nugget honeymoon suite at Willow Lake Bed & Breakfast Inn offers custom packages in romantic suroundings. A honeymooner's dream come true.\" />";

include("../res/header.php");?>

<div id="content">

<h3 class="rightf"><a href="/reserve/"><img src="/res/images/buttons/reserve" width="145" height="60" alt="Reservations" /></a></h3>
<h2>Honeymoon Suite</h2>
<p>When the occasion merits nothing but the best, our 
<strong>Gold Nugget Suite</strong><br> 
exceeds all expectations. Inquire about custom packages.</p>

<div id="rows2">
<div class="leftf bounds">
<ul>
	<li>King-sized feather bed</li>
	<li>Ultra High Definition 3D TV</li>
	<li>Private bath with shower</li>
	<li>Personal jacuzzi</li>
	<li>Complimentary amenities</li>
</ul>
</div>
<div class="leftf bounds right pull">
<span>$219&nbsp;&nbsp;&nbsp;&nbsp;</span><br>
<a href="/reserve/?room=honeymoon"><img src="/res/images/buttons/reserveblack" width="100" height="50" alt="Reserve Honeymoon Suite" /></a>
</div>
</div>

<div id="dynamic" class="block pull">
	<img class="border leftf" src="/res/images/accommodations/honeymoon" alt="Honeymoon Suite" width="385" height="300" />
	<img class="border leftf" src="/res/images/accommodations/jacuzzi" alt="Jacuzzi" width="385" height="300" />
</div>

<div id="carouselcontent" class="hide">

<span class="prev">&lt;&lt;</span>
<span class="next">>></span>

<ul>
	<li>
	<img class="border" src="/res/images/accommodations/honeymoon" alt="Honeymoon Suite" width="385" height="300" />
	</li>
	<li>
	<img class="border" src="/res/images/accommodations/jacuzzi" alt="Jacuzzi" width="385" height="300" />
	</li>
	<li>
	<img class="border" src="/res/images/accommodations/champagne" alt="Champagne Glasses" width="385" height="300" />
	</li>
</ul>
</div>
<p class="center"><a href="lower-level">Lower Level</a> · <a href="upper-level">Upper Level</a></p>
</div>

<script type="text/javascript" src="/res/scripts/jquery.js"></script>
<script type="text/javascript" src="/res/scripts/jcarousellite.js"></script>

<script>
var content = document.getElementById("carouselcontent");
var box = document.getElementById("dynamic");
box.id = "carousel";
box.className = "carousel";
box.innerHTML = content.innerHTML;

$(function() {
    $(".carousel").jCarouselLite({
        btnNext: ".next",
        btnPrev: ".prev"
    });
});
</script>

<?php include("../res/footer.php"); ?>
