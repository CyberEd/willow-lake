<?php $title="Rooms"; 

$meta = "<meta name=\"description\" content=\"Five star country chic accommodations fullfill each guest's individual need. Pets are welcome!\" />";

include("../res/header.php");?>

<div id="content">

<p><span class="dropcap">O</span>ur country chic accommodations are tailored
to meet the individual needs of our guests. We feature four cozy 
rooms on the lower level with shared bathrooms as well as spacious
sleeping areas with individual baths on the second floor. 
Our Honeymoon Suite boasts an exclusive private bath with jacuzzi. 
All rooms are furnished with a fireplace or wood stove.</p>

<ul>
	<li>All rooms non-smoking</li>
	<li>Pets welcome</li>
	<li>WiFi in every room</li>
	<li>Extended Stay Discounts</li>
	<li>Rates: $110 - 219</li>
</ul>

<div id="imgcontainer">
<br><br>
<ul>
	<li><a href="lower-level"><img class="border" src="/res/images/lower_level" alt="Lower Level Rooms" width="172" height="178" /></a></li>
	<li><a href="upper-level"><img class="border" src="/res/images/upper_level" alt="Upper Level Rooms" width="172" height="178" /></a></li>
	<li><a href="honeymoon-suite"><img class="border" src="/res/images/honeymoon" alt="Honeymoon Suite" width="172" height="178" /></a></li>
</ul>

<p class="lbl lower">Lower Level</p>
<p class="lbl upper">Upper Level</p>
<p class="lbl honeymoon">Honeymoon Suite</p>

</div>
<br>
</div>

<?php include("../res/footer.php"); ?>
