<?php $title="Willow Lake"; 

$meta = "<meta name=\"description\" content=\"Looking for the perfect getaway vacation? Willow Lake Bed & Breakfast Inn nestled in the foothills of Wyoming's Big Horn Mountains is for you.\" />";

include("res/header.php"); ?>

<div id="content">

<div id="slideshow">
<ul>
	<li><img src="/res/images/home/willowlake.jpg" alt="Landscape" height="400" width="100%" /></li>
	<li><img src="/res/images/home/lodge.jpg" alt="Willow Lake Lodge" height="400" width="100%" /></li>
	<li><img src="/res/images/home/thermalpool.jpg" alt="Hot Springs Pool" height="400" width="100%" /></li>
	<li><img src="/res/images/home/tea.jpg" alt="Tea Party" height="400" width="100%" /></li>
	<li><img src="/res/images/home/bedroom.jpg" alt="Bedroom" height="400" width="100%" /></li>
	<li><img src="/res/images/home/grand.jpg" alt="Grand Room" height="400" width="100%" /></li>
</ul>
<span class="arrow previous"></span>
<span class="arrow next"></span>
</div>

<div class="text">
<h1>Welcome</h1>
<p><span class="dropcap">W</span>illow Lake, the perfect location for
your <strong>getaway</strong>, is nestled in the panoramic foothills of Wyoming's Big 
Horn Mountains. Our <strong>award winning</strong> bed and breakfast offers a variety 
of accommodations and exquisite meals prepared by our own renowned Chef
Louis. Indulge in a healing soak in our natural hot springs mineral pool.
Treat yourself to a soothing massage.</p>

<p>Discover your <strong>outdoor adventure</strong> whichever season you choose to visit.
Spring and summer offer fishing, boating, hiking, and horseback
riding. Fall and winter activities include skiing and dogsledding.
Your passion is our priority.</p>

<p>Delight in a customized <strong>&ldquo;Tea Party&rdquo;</strong> with all
the amenities to celebrate that memorable occasion. Our top notch, 
friendly staff invite you to experience a romantic getaway, family 
vacation, weekend escape, or special event at our historic resort.</p>
<h3>Relax, stay awhile, rejuvenate amongst the mystical spirit of the
whispering willows.</h3>
</div>

<div class="rightcontent">
<div class="verticaldelay centercontainer">
<div class="centerfixer">
<img src="/res/images/awards/catering" alt="Catering Award" width="125" height="70" />
<p class="center quote">Your wonderful staff turned my Tea Party Bridal Shower into a 
fantasy dream come true. With cherished memories,<br>~Abigail Kent, Vail, CO.</p>
<img src="/res/images/awards/pancake" alt="Top Pancakes Award" width="125" height="90" />
<p class="center quote">Mmmm... crêpes au chocolat, croissants with pot au creme, even 
better than my Aunt Nicole's. Thank you, Chef Louis!<br>~The Claubards, Quebec, Canada</p>
<img src="/res/images/awards/host" alt="Best Host Award" width="125" height="110" />
<p class="center quote">Friendly, down home staff welcomed us like family.<br>
~John and Becky Novak, Houston, TX</p>
</div>
</div>

</div>
<!--Add extra space for high DPI-->
<br>
</div>

<!--Slide Show Scripts-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script src="/res/scripts/slideshow.js"></script>
<script src="/res/scripts/slideshowauto.js"></script>
<!--End Slideshow-->
<?php include("res/footer.php"); ?>
