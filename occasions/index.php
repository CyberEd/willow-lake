<?php $title="Tea Parties"; 

$meta = "<meta name=\"description\" content=\"For a unique way to celebrate that special occasion, consider a custom tea party at Willow Lake Bed & Breakfast Inn.\" />";

include("../res/header.php");?>

<div id="content">

<p><span class="dropcap">O</span>ur knowledgeable event planners can help 
you customize a complete venue for your perfect social soirée to celebrate that 
special occasion.</p>

<h1>Tea Parties</h1>

<div class="center"><img class="bold" src="/res/images/teapot" alt="Teapot Graphic" width="275" height="200" /></div>
<h2>Themes</h2>
<ul>
	<li>Bridal/Baby Showers</li>
	<li>Anniversaries</li>
	<li>Birthdays</li>
	<li>Mother and Daughter</li>
	<li>Ladies' Luncheon</li>
</ul>

<div><img class="right border" src="/res/images/japan" alt="Japanese Tea Ceremony" width="350" height="220" /></div>
<h2>Japanese Tea Ceremony</h2>
<ul>
	<li>Organic Ti Kuan Yin teas</li>
	<li>Fresh fruits</li>
	<li>Salmon cress &ldquo;fingerwiches&rdquo;</li>
	<li>Cucumber pinwheels</li>
	<li>Green tea cupcakes
		<ul>
		<li>Gluten-free</li>
		</ul>
	</li>
</ul>
<br>
<div style="margin-left: 10px;"><img class="right border" src="/res/images/english" alt="English Tea Ritual" width="350" height="220" /></div>
<h2>English Tea Ritual</h2>
<ul>
	<li>Mariage Freres Teas</li>
	<li>Scones with homemade jam</li>
	<li>Crumpets with clotted cream</li>
	<li>Assorted cheeses and fruits</li>
	<li>Petitfours</li>
</ul>

<br>

<p>Copious cups of tea are served in Grandma Evelyn's antique tea sets 
collected from her worldwide travels. Weather permitting, events can be 
held outdoors in the formal <a href="garden">Eve's Garden</a>.</p>

<h3>Ask for Cyndi, Event planner</h3>

</div>

<?php include("../res/footer.php"); ?>
