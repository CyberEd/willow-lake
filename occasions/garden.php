<?php $title="Eve's Garden"; include("../res/header.php");?>

<div id="content">

<img class="center border" src="/res/images/gazebo" alt="Beautiful gazebo in the garden" width="600" height="320" />

<p><span class="dropcap">F</span>ill your senses with the Glacier Lily, 
Scarlet Rose, coral Fairy Trumpet, magenta Shooting Star, and vivid Indian
Paintbrush of Eve's Garden.</p>

<p>Named in memory of Grandma Evelyn, this magical, peaceful retreat 
offers a captivating backdrop for your special occasion. The Gazebo 
provides an ideal sanctuary to host a wedding, anniversary, family 
reunion, or birthday celebration amongst the serenity of Willow Lake.</p>

<p>We <strong>guarantee</strong> white glove attention to every detail of your event from
catering to accommodations. Eve's Garden; a little piece of heaven on 
Earth.</p>

<img class="center border" src="/res/images/collage" alt="A collage of opportunity" width="600" height="320" />

</div>
<?php include("../res/footer.php"); ?>
