<?php $title="Dedication"; include("../res/header.php");?>

<div id="content">

<h3>Dedication</h3>
<img class="center" src="/res/images/dedication" alt="Grandma Evelyn" width="400" height="300" />
<p class="center">1915-2012</p>
<p>Yes, there actually was a Grandma Evelyn. She walked this Earth for
97 years. She would have loved Willow Lake. This site is dedicated to her
memory.</p>
<br>
<hr>
<div class="leftf"><a href="development">Development</a></div>
<br>
</div>
<?php include("../res/footer.php"); ?>
