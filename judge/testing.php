<?php $title="Testing"; include("../res/header.php");?>

<div id="content">

<h3>Testing</h3>
<p>Browsers:</p>
<ul>
	<li>Internet Explorer VERSION</li>
	<li>Firefox</li>
	<li>Google Chrome</li>
	<li>Opera</li>
	<li>Safari</li>
</ul>
<p>Platforms:</p>
<ul>
	<li>Desktop</li>
	<li>Android Phone</li>
	<li>Android Tablet</li>
	<li>iPhone</li>
	<li>Surface</li>
</ul>
<p>W3C validator utilized as a tool.</p>
<p><a href="http://www.google.com/analytics/">Google Analytics</a> used for statistic gathering.</p>
<br>
<hr>
<div class="leftf"><a href="development">Development</a></div>
<div class="rightf"><a href="credits">Credits</a></div>
<br>
</div>
<?php include("../res/footer.php"); ?>
