<?php $title="Development"; include("../res/header.php");?>

<div id="content">

<h3>Development</h3>
<p>This website has been <strong>100%</strong> custom built from the ground up. There has been <strong>no use</strong> of stylistic templates. Several programming languages were used during development including:</p>
<ul>
	<li>HTML 5</li>
	<li>JavaScript with AJAX</li>
	<li>CSS</li>
	<li>PHP</li>
	<li>SQL</li>
</ul>
<p>Software</p>
<ul>
	<li><a target="_blank" href="http://gimp.org">GIMP:</a> Image Editor</li>
	<li><a target="_blank" href="http://en.wikipedia.org/wiki/Git_(software)">Git:</a> Version Control</li>
	<li><a target="_blank" href="http://www.vim.org/index.php">Vim:</a> Text editor</li>
</ul>
<p>In an attempt to stay current, Flash (ActionScript) has been intentionally avoided. Accounting for the recent mobile movement, Flash is rapidly becoming an outdated technology.</p>
<p>Note: The <a href="/reserve/">online reservations</a> are fully functional. Some info has been intentionally disabled, as this is a fictitious establishment. Input your name and valid email to receive confirmation.</p>
<p>Enabling JavaScript is recommended to view interactive content.</p>
<p>CSS and HTML 5 were implemented throughout the majority of the site to optimize accessibility.</p>
<p>CSS file not minimalized to maintain readability</p>

<p><a target="_blank" href="https://bitbucket.org/CyberEd/willow-lake/src">View Code on Bitbucket</a></p>
<br>
<hr>
<div class="leftf"><a href="research">Research</a></div>
<div class="rightf"><a href="testing">Testing</a></div>
<br>
</div>
<?php include("../res/footer.php"); ?>
