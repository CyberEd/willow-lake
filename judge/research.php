<?php $title="Research"; include("../res/header.php");?>

<div id="content">

<h3>Research</h3>
<p>Willow Lake Bed & Breakfast is based upon historical fiction. Months of research included the following topics:
<ul>
	<li>1800s southern miners and their move west, specifically Georgia</li>
	<li>Architectural elements of the 1800s, specifically Antebellum structure and furnishings</li>
	<li>Illnesses of the 1800s</li>
	<li>French cuisine, language, and recipes</li>
	<li>Tea ceremonies from both England and Japan</li>
	<li>Wyoming birds</li>
	<li>Mountain foliage and wildflowers</li>
	<li>The pussy willow: facts and lore</li>
	<li>Other bed and breakfasts/sites</li>
</ul>
<br>
<hr>
<div class="leftf"><a href="index">Conception</a></div>
<div class="rightf"><a href="development">Development</a></div>
<br>
</div>
<?php include("../res/footer.php"); ?>
