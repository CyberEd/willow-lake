<?php $title="Conception"; include("../res/header.php");?>

<div id="content">

<h2>FBLA Website Design Entry 2014</h2>
<h3>Conception</h3>

<p>Upon first reading the prompt, images of sweeping willow trees 
surrounding a lake and a southern Victorian style mansion first came to 
mind. However, hailing from rural Wyoming surrounded by brush, mountains,
and moose, we had a different take on Willow Lake Bed and Breakfast. We
<strong>envisioned</strong> tall pussy willows and cattails so prevalent 
in our area, surrounding a mountain lake and hot springs.</p>

<p>Staying within the parameters of the prompt and creating a bed and 
breakfast specific to our local as well as global audience was 
<strong>challenging</strong>. Our goal was to create
a structure with <strong>character</strong> and <strong>history</strong>
within the 1800s time frame. Much research brought us to embrace the
Antebellum architectural style. But how to justify a southern mansion
in the foothills of Wyoming? The infinite quest for gold and Grandpa
Frank's &ldquo;story&rdquo; provided the perfect explanation.</p>

<h3>Willow Lake Bed & Breakfast Inn was born.</h3>
<br>
<hr>
<div class="rightf"><a href="research">Research</a></div>
<br>
</div>
<?php include("../res/footer.php"); ?>
