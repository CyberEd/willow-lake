<?php $title="Credits"; include("../res/header.php");?>

<div id="content">

<h3>Credits</h3>
<p>A very special thanks to Anne Gunn & the <a target="_blank" href="http://sherprog.com/">Sheridan Programmers Guild</a> for making the hosting of this website possible.</p>

<h4>Fonts</h4>
<ul>
	<li><a href="https://www.google.com/fonts">Google Fonts</a>
	<ul>
		<li>Courgette</li>
		<li>Indie Flower</li>
	</ul></li>
	<li><a href="http://www.fontsquirrel.com/">Font Squirrel</a>
	<ul>
		<li>Shangri La</li>
	</ul></li>
</ul>

<h4>Photography</h4>
<p>The photographs below are from <strong>personal archives</strong>.</p>
<ul>
	<li><a href="/res/images/home/tea.jpg">Tea Party</a></li>
	<li><a href="/res/images/home/grand.jpg">Grand Room</a></li>
	<li><a href="/res/images/lower_level">Lower Level</a></li>
	<li><a href="/res/images/accommodations/primrose">Primrose Room</a></li>
	<li><a href="/res/images/accommodations/bathroom2">Bathroom</a></li>
	<li><a href="/res/images/accommodations/silvermoon">Silvermoon Room</a></li>
	<li><a href="/res/images/accommodations/sunburst">Sunburst Room</a></li>
	<li><a href="/res/images/accommodations/bathroom">Bathroom 2</a></li>
	<li><a href="/res/images/accommodations/hideaway">Ed's Retreat</a></li>
	<li><a href="/res/images/accommodations/jacuzzi">Jacuzzi</a></li>
	<li><a href="/res/images/dining">Sweetwater Dining Room</a></li>
	<li><a href="/res/images/recipes/quiche">Quiche Breakfast</a></li>
	<li><a href="/res/images/recipes/scones">Scones</a></li>
	<li><a href="/res/images/japan">Japanese Tea Ceremony</a></li>
	<li><a href="/res/images/english">English Tea Ritual</a></li>
	<li><a href="/res/images/info/owl.jpg">Owl</a></li>
	<li><a href="/res/images/info/moose.jpg">Moose</a></li>
	<li><a href="/res/images/info/fish.jpg">Fishing</a></li>
	<li><a href="/res/images/info/pheasant.jpg">Pheasant</a></li>
</ul>
<p>The remainder are from a <a href="http://www.canstockphoto.com/">CanStockPhoto</a> subscription.</p>

<p>All proper documentation and copyright laws were adhered to in the creation of this website.</p>
<br>
<hr>
<div class="leftf"><a href="testing">Testing</a></div>
<div class="rightf"><a href="dedication">Dedication</a></div>
<br>
</div>
<?php include("../res/footer.php"); ?>
