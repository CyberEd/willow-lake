<?php $title="Our Story"; 

$meta = "<meta name=\"description\" content=\"Original settlement claimed in 1853. In the 1900s the Topels renovate the antebellum property establishing Willow Lake Bed & Breakfast Inn.\" />";

include("res/header.php");?>

<div id="content">

<h1>Our Story</h1>

<br>

<p><span class="dropcap">I</span>n 1849, word of the great California 
gold strikes reached the mines of Georgia and like many others from 
Habersham, Frank and Evelyn Topel and their two young sons headed west to 
seek their fortune.</p>

<p>Struck by the breathtaking beauty of the Big Horn Mountains and weary 
of travel, the family settled in Wyoming. In 1853, Frank struck gold along
the Sweetwater River and eventually purchased a plot of land near Willow 
Lake. Evelyn, suffering from catarrh proclaimed the healing powers of the 
nearby natural hot springs. Their new home was constructed in the 
antebellum architectural style so reminiscent of their Georgian roots. 
Evelyn always took great pride in flourishing the homestead with pussy willow
branches gathered from the lake's edge. A first sign of spring, they were 
a much welcomed sight after the long, harsh Wyoming winters.</p>

<img class="center border" src="/res/images/1900s" alt="Original image of the 1900s homestead" width="450" height="300" />

<p>In the 1900s Burton Topel assumed ownership, maintaining and restoring
the homestead. In 1987, my wife Susan and I took possession of the property
with the intent of turning it into a bed and breakfast. The estate was 
doubled in size undergoing massive reconstruction with the edition of a 
hot springs pool, lavish patio area, and separate private residence. 
Special care was taken to maintain the original antebellum elements and 
furnishings.</p>

<p>It is my family's dream that the spirit of Frank and Evelyn will delight 
in the joy and laughter of our welcomed guests from around the globe. 
May the unexpected willow branches you encounter here and there throughout
your stay be a reminder to always keep the memories of Willow Lake and 
springtime in your heart.</p>

<h3>We welcome you with open arms. Let us know how we can make your stay 
most enjoyable.</h3>

<p>Your hosts,</p>
<p>Edward & Susan</p>

</div>
<?php include("res/footer.php"); ?>
