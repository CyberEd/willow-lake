$(window).load(function(){

    // The window.load event guarantees that
    // all the images are loaded before the
    // auto-advance begins.

    var timeOut = null;

    $('#slider .arrow').click(function(e,simulated){

        // The simulated parameter is set by the
        // trigger method.

        if(!simulated){

            // A real click occured. Cancel the
            // auto advance animation.

            clearTimeout(timeOut);
        }
    });

    // A self executing named function expression:

    (function autoAdvance(){

        // Simulating a click on the next arrow.
        $('#slider .next').trigger('click',[true]);

        //timeOut time in ms
	timeOut = setTimeout(autoAdvance,3000);
    })();

});
