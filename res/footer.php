<div id="footer">

<div class="left">
<p><strong>(307) 622-1492</strong><br>
<a href="mailto:go@visitwillowlake.com">go@visitwillowlake.com</a></p>
<p>119 Catkin Lane<br>
Willow Lake, WY 82801<p>
</div>

<img class="center" alt="Circle Logo" src="/res/images/logo_circle.png" height="150" width="150" />
<p class="center">Willow Lake<br>Bed & Breakfast Inn</p>
<p class="center fontreset"><em>BHHS Wyoming Web Development Team &copy; 2014</em></p>

<div class="right">
<h3><a href="/judge/">About this site</a></h3>
</div>

</div>
<br>
<!--Dropdown Menu JS-->
<script type="text/javascript" src="/res/scripts/dropdown.js"></script>
<script type="text/javascript">
var dropdown=new TINY.dropdown.init("dropdown", {id:'menu', active:'menuhover'});
</script>
<!--Google Analytics-->
<script type="text/javascript" src="/res/scripts/analytics.js"></script>
</body>
</html>
