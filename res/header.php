<?php session_start(); ?>
<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<?php echo($meta); ?>

<title><?php echo $title; ?></title>
<link rel="shortcut icon" href="/res/favicon.ico" />
<link href="/res/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--Begin Header-->
<div id="header">
<a href="/"><img class="logo" src="/res/images/logo.png" alt="Willow Lake Logo" height="200" width="335" /></a>
<h1 class="title">Willow Lake</h1>
<!-- <img class="spacer" src="/res/images/hr.gif" /> -->
<h3 class="subtitle">Bed & Breakfast Inn</h3>
</div>

<div id="social">
<div class="container">
<a class="youtube" target="_blank" href="http://youtu.be/7DBiS5HUk1g"></a>
<a class="facebook" target="_blank" href="https://www.facebook.com/visitwillow"></a>
<a class="plus" target="_blank" href="http://plus.google.com/u/0/b/105977310362269412910/105977310362269412910/about/p/pub"></a>
<a class="twitter" target="_blank" href="http://twitter.com/visitwillow"></a>
</div>
</div>

<!--Create Drop Down Menu-->
<div id="navbar">
<div class="nav">
<ul id="menu" class="menu">
	<li class="nodiv"><a href="/">Home</a></li>
	<li class="overflow"><a href="/accommodations/">Accommodations</a>
		<ul>
		<li><a href="/accommodations/">Rooms</a></li>
		<li><a href="/accommodations/lower-level">Lower Level</a></li>
		<li><a href="/accommodations/upper-level">Upper Level</a></li>
		<li><a href="/accommodations/honeymoon-suite">Honeymoon Suite</a></li>
		</ul>
	</li>
	<li class="short"><a href="/dining">Dining</a>
		<ul>
		<li><a href="/dining/">Breakfast</a></li>
		<li><a href="/dining/menu">Sample Menu</a></li>
		<li><a href="/dining/recipe">Recipes</a></li>
		</ul>
	</li>
	<li><a href="/occasions/">Occasions</a>
		<ul>
		<li><a href="/occasions/">Tea Party</a></li>
		<li><a href="/occasions/garden">Eve's Garden</a></li>
		</ul>
	</li>
	<li><a href="/info/">Visitor Info</a>
	<ul>
	<li><a href="/info/">Out & About</a></li>
	<li><a href="/info/shop">Gift Shop</a></li>
	</ul>
	</li>
	<li class="nodiv"><a href="/about">Our Story</a></li>
	<li class="nodiv"><a href="/reserve/">Reservations</a></li>
</ul>
</div>
</div>
